<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TicketController;
use App\Http\Controllers\CommentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('profile', function () {
    return Auth::user()->name;
})->middleware('auth');

Route::prefix('admin')->group(function () {
    Auth::routes(['register' => false]);
});

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('tickets', 'TicketController')->only('index', 'create', 'store');

Route::get('tickets/status', [TicketController::class, 'status'])->name('tickets.status');

Route::get('tickets/details', [TicketController::class, 'details'])->name('tickets.details');

Route::get('tickets/{reference}', [TicketController::class, 'show'])->name('tickets.show');

Route::post('tickets/{reference}/comments', [CommentController::class, 'store'])->name('comments.store');

Route::livewire('/counter', 'counter');








