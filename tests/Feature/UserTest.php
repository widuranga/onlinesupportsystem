<?php
use Illuminate\Foundation\Testing\RefreshDatabase;

uses(RefreshDatabase::class);

it('user is in the db',function (){
    factory(App\User::class)->create();
    //$this-
    //actingAs($user)->get('/profile')->assertSee($user->name);
    $this->assertDatabaseHas('users',['id'=>1]);
});
