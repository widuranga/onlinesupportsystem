<?php

namespace App\Traits;


use Mail;

trait EmailManager
{
    public function sendEmail($result, $type)
    {
        if ($type == 1) {
            $template = 'emails.ticket_verification';
            $subject='Ticket Verification';
            $user=$result->email;
        } else {
            $template = 'emails.reply';
            $subject='Comment';
            $user=$result['email'];
        }
        Mail::send($template,
            [
                'result' => $result,
                'subject'=>$subject,
                'user'=>$user
            ],
            function ($m) use (
                $result,
                $subject,
                $user
            ) {
                $m->from('orders@ticket.com', 'Customer Ticket - acknowledgement');
                $m->to($user);
                $m->subject($subject);
            });

    }

}
