<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ticket extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'description',
        'email',
        'phone',
        'status',
    ];

    protected static function booted()
    {
        static::creating(function ($model) {
            $model->reference = (string)Str::uuid();
        });
    }

//    public function setPhoneAttribute($value)
//    {
//        if (isset($value)) {
//            $this->attributes['phone'] = phone($value, ['AUTO', 'LK'], 'E164');
//        }
//    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }
}

