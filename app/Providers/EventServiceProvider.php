<?php

namespace App\Providers;

use App\Listeners\TicketReplyListener;
use App\Events\TicketReplyEvent;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        TicketCreatedEvent::class => [
            TicketCreatedListener::class,
        ],TicketReplyEvent::class => [
            TicketReplyListener::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
