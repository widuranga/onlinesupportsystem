<?php

namespace App\Listeners;

use App\Events\TicketCreatedEvent;
use App\Mail\TicketCreatedMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class TicketCreatedListener implements ShouldQueue
{
    use InteractsWithQueue;

    public function handle(TicketCreatedEvent $event)
    {
        Mail::to($event->ticket->email)
            ->send(new TicketCreatedMail($event->ticket));
    }
}
