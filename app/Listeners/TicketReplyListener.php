<?php

namespace App\Listeners;

use App\Events\TicketReplyEvent;
use App\Mail\TicketReplyMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class TicketReplyListener implements ShouldQueue
{
    use InteractsWithQueue;

    public function handle(TicketReplyEvent $event)
    {
        Mail::to($event->comment->ticket->email)
            ->send(new TicketReplyMail($event->comment));
    }
}
