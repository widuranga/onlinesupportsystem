<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Ticket;

class Counter extends Component
{
    public $tickets;

    public function render()
    {
        return view('livewire.counter');
    }

    public function mount()
    {
        $this->tickets = Ticket::all();
    }

}
