<?php

namespace App\Http\Controllers;

use App\Ticket;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Http\Requests\TicketRequest;
use App\Events\TicketCreatedEvent;
use Illuminate\Support\Facades\DB;

class TicketController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->only('index', 'reply');
    }

    public function index(Request $request)
    {
        $tickets = Ticket::query();

        if ($request->has('search')) {
            $tickets = $tickets->where('name', 'LIKE', $request->search . '%');
        }

        $tickets = $tickets->orderBy('status')->simplePaginate(15);

        return view('tickets.index')
            ->withTickets($tickets);
    }

    public function create()
    {
        return view('tickets.create');
    }

    public function store(TicketRequest $request)
    {
        DB::beginTransaction();
        try {
            $ticket = Ticket::create([
                'name' => $request->get('name'),
                'description' => $request->get('description'),
                'email' => $request->get('email'),
                'phone' => $request->get('phone'),
                'status' => 1
            ]);
            DB::commit();

            event(new TicketCreatedEvent($ticket));

            return redirect()->route('tickets.show', $ticket->reference);
        } catch (QueryException $exception) {
            DB::rollback();
        }

        return redirect()->route('tickets.create');
    }

    public function show($ticket)
    {
        $ticket = Ticket::where(['reference' => $ticket])->first();
        return view('tickets.show', compact('ticket'));
    }

    public function status()
    {
        return view('tickets.status');
    }

    public function details(Request $request)
    {
        $ticket = Ticket::where(['reference' => $request->reference])->first();
        if ($ticket) {
            return $ticket;
        }
        return [];
    }

}
