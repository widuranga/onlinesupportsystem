<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Ticket;
use App\Events\TicketReplyEvent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    public function store(Request $request)
    {
        $response = collect();
        try {
            $comment = Comment::create($request->all());
            if (Auth::check()) {
                $comment->user_id = Auth::id();
                $comment->ticket->status = 2;
            } else {
                $comment->ticket->status = 3;
            }
            $comment->save();
            $comment->ticket->save();
            event(new TicketReplyEvent($comment));
            $response->put('title', 'Successful');
            $response->put('text', 'Operation Successful');
            $response->put('type', 'success');
        } catch (\Exception $exception) {
            $response->put('title', 'Warning');
            $response->put('text', 'Something went wrong please try again');
            $response->put('type', 'warning');
        }
        return $response;
    }
}
