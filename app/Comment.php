<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Comment extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'ticket_id',
        'comment'
    ];

    public function ticket()
    {
        return $this->belongsTo(Ticket::class);
    }

}
