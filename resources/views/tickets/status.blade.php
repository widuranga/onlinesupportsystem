@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm">
                {!! Form::text('Enter Reference No:','',['id'=>'ref_no','name'=>'ref_no','class'=>'form-control']) !!}
            </div>
            <div class="col-sm">
                <a href="#" onclick="getStatus()" class="btn btn-info">Search</a>
            </div>
        </div>
        <div style="display: none" id="result">
        <div class="row">
            <div class="col-sm">
                Ticket Status:
            </div>
            <div class="col-sm" id="status">

            </div>
        </div>
        <div class="row">
            <div class="col-sm">
                Name:
            </div>
            <div class="col-sm" id="name">

            </div>
        </div>

        <div class="row">
            <div class="col-sm">
                Email:
            </div>
            <div class="col-sm" id="email">

            </div>
        </div>

        <div class="row">
            <div class="col-sm">
                Phone:
            </div>
            <div class="col-sm" id="phone">

            </div>
        </div>
        <div class="row">
            <div class="col-sm">
                Description:
            </div>
            <div class="col-sm" id="dis">

            </div>
        </div>
        </div>
    </div>
@stop
@section('after-scripts-end')
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>

        function getStatus() {
            const ref_no = $('input[name=ref_no]').val();

            if ($('#reply').val() != '') {
                axios.get('{{route('tickets.details')}}', {
                    params: {
                        reference: ref_no
                    }
                })
                    .then(function (response) {
                        if(response.data) {
                            if (response.data.status == 1) {
                                $('#status').html('<span class="badge badge-primary">New</span>')
                            } else if (response.data.status == 2) {
                                $('#status').html('<span class="badge badge-success">In progress</span>')
                            } else {
                                $('#status').html('<span class="badge badge-warning">Reply</span>')
                            }
                            $('#name').text(response.data.name)
                            $('#email').text(response.data.email)
                            $('#phone').text(response.data.phone)
                            $('#dis').text(response.data.description)
                            $('#result').show()
                        }
                    })
                    .catch(function (error) {
                        console.log(error);
                    })
                    .then(function () {
                        // always executed
                    });

            } else {

            }
        }

    </script>
@endsection
