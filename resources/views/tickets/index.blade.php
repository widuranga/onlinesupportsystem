@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">

            <div class="col-md-12">
                <form>
                    <div class="col-md-6 {{ $errors->has('name') ? ' has-error' : '' }}">
                        {!! Form::text('search','',['class'=>'form-control']) !!}
                    </div>

                    <div class="col-md-6 {{ $errors->has('name') ? ' has-error' : '' }}">
                        <button class="btn btn-primary btn-lg btn-block" type="submit">Submit</button>
                    </div>
                </form>
            </div>

            <div class="col-md-12">

                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>Customer Name</th>
                        <th>Email</th>
                        <th>Phone Number</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($tickets as $value)
                        <tr>
                            <td>{{$value->name}}</td>
                            <td>{{$value->email}}</td>
                            <td>{{$value->phone}}</td>
                            <td>
                                @if($value->status==1)
                                    <span class="badge badge-primary">New</span>
                                @elseif($value->status==2)
                                    <span class="badge badge-success">In progress</span>
                                @else
                                    <span class="badge badge-warning">Reply</span>
                                @endif

                            </td>
                            <td><a href="{{route('tickets.show',$value->reference)}}" class="btn btn-info">view</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{ $tickets->links() }}
            </div>
        </div>
    </div>
@endsection
