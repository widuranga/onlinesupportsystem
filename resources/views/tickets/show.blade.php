@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm">
                Ticket Status:
            </div>
            <div class="col-sm">
                @if($ticket->status==1)
                    <span class="badge badge-primary">New</span>
                @elseif($ticket->status==2)
                    <span class="badge badge-success">In progress</span>
                @else
                    <span class="badge badge-warning">Reply</span>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-sm">
                Name:
            </div>
            <div class="col-sm">
                {{$ticket->name}}
            </div>
        </div>

        <div class="row">
            <div class="col-sm">
                Email:
            </div>
            <div class="col-sm">
                {{$ticket->email}}
            </div>
        </div>

        <div class="row">
            <div class="col-sm">
                Phone:
            </div>
            <div class="col-sm">
                {{$ticket->phone}}
            </div>
        </div>
        <div class="row">
            <div class="col-sm">
                Description:
            </div>
            <div class="col-sm">
                {{$ticket->description}}
            </div>
        </div>

        <div class="row">

            <div class="col-sm">
                <textarea id="reply" class="form-control"></textarea>
            </div>

        </div>


        <div class="row">
            <div class="col-sm">

            </div>
            <div class="col-sm">
                <a href="#" onclick="reply()" class="btn btn-info">Reply</a>
            </div>
            <div class="col-sm">

            </div>
        </div>

        @foreach($ticket->comments as $comment)
            @if($comment->user_id!='')
                <div class="row">
                    <div class="col-sm">
                        <span class="badge badge-primary">Agent</span>
                    </div>
                    <div class="col-sm">
                        {{$comment->comment}}
                    </div>
                </div>
            @else
                <div class="row">
                    <div class="col-sm">
                        {{$comment->comment}}
                    </div>
                    <div class="col-sm">
                        <span class="badge badge-success">Customer</span>
                    </div>
                </div>
            @endif
        @endforeach
        <input type="hidden" name="_token" value="UazwHpzsAB5gnGPLGd2u7OEtxc7rkfIfKZ4uQ67z">
    </div>
@stop
@section('after-scripts-end')
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>

        function reply() {
            const token = $('input[name=_token]').val();

            if ($('#reply').val() != '') {
                axios.post('{{route('comments.store', $ticket->reference)}}', {
                    comment: $('#reply').val(),
                    ticket_id: {{ $ticket->id }},
                })
                    .then((response) => {
                            swal({
                                title: response.data.title,
                                text: response.data.text,
                                icon: response.data.type,
                                buttons: true,
                                dangerMode: true,
                            })
                                .then((willDelete) => {
                                    if (willDelete) {
                                        location.reload();
                                    } else {
                                        // location.reload();
                                    }
                                });


                        },
                        (error) => {
                            console.log(error);
                        }
                    );

            } else {

            }
        }

    </script>
@endsection
