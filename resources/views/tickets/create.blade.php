@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="py-5 text-center">
            <h2>Create Tickt</h2>
            {{--            <p class="lead">Below is an example form built entirely with Bootstrap's form controls. Each required form group has a validation state that can be triggered by attempting to submit the form without completing it.</p>--}}
        </div>

        <div class="row">
            <div class="col-md-12">
                {!! Form::open(['route' => 'tickets.store', 'files' => true, 'id' => 'create-ticket']) !!}

                <div class="mb-3 {{ $errors->has('name') ? ' has-error' : '' }}">
                    {!! Form::text('Name','',['name'=>'name','class'=>'form-control']) !!}
                    @if ($errors->has('name'))
                        <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
                    @endif
                </div>

                <div class="row">

                    <div class="col-md-6 mb-3 {{ $errors->has('email') ? ' has-error' : '' }}">
                        {!! Form::text('Email','',['name'=>'email','class'=>'form-control']) !!}
                        @if ($errors->has('email'))
                            <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
                        @endif
                    </div>

                    <div class="col-md-6 mb-3 {{ $errors->has('phone') ? ' has-error' : '' }}">
                        {!! Form::text('Phone','',['name'=>'phone','class'=>'form-control']) !!}
                        @if ($errors->has('phone'))
                            <span class="help-block"><strong>{{ $errors->first('phone') }}</strong></span>
                        @endif
                    </div>

                </div>

                <div class="mb-3 {{ $errors->has('description') ? ' has-error' : '' }}">
                    {!! Form::textarea('Description','',['name'=>'description','class'=>'form-control']) !!}
                    @if ($errors->has('description'))
                        <span class="help-block"><strong>{{ $errors->first('description') }}</strong></span>
                    @endif
                </div>

                <button class="btn btn-primary btn-lg btn-block" type="submit">Submit</button>
                {!! Form::close() !!}
            </div>
        </div>

        <footer class="my-5 pt-5 text-muted text-center text-small">
            <p class="mb-1">© 2017-2018 Company Name</p>
        </footer>
    </div>
@endsection
