<?php

return [

    'status' => [
        1 => 'New',
        2 => 'In progress',
        3 => 'Reply'
    ]
];
